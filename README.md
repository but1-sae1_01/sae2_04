# SAE2_04 - Exploitation d’une base de données


- Concevoir une base de données relationnelle à partir d’un cahier des charges,

- Mettre à jour et interroger une base de données relationnelle,

- Visualiser des données.