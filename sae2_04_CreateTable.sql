--on a mit modulee car il n'accepte pas module
CREATE TABLE Modulee( 
   codemod VARCHAR(5),
   nomMod VARCHAR(150) not null,
   polemod VARCHAR(3) not null,
   domainemod VARCHAR(25) not null,
   primary key(codemod),
   constraint pb_codemod check (codemod in ('R1.01', 'R1.02','R1.03','R1.04','R1.05','R1.06','R1.07','R1.08','R1.09','R1.10','R1.11','R1.12','R2.01','R2.02','R2.03','R2.04','R2.05','R2.06','R2.07','R2.08','R2.09','R2.10','R2.11','R2.12','R2.13','R2.14','P2.01','S1.01','S1.02','S1.03','S1.04','S1.05','S1.06','S2.01','S2.02','S2.03','S2.04','S2.05','S2.06')),
   --comme indiqué dans le sujet on se base QUE sur le S1 et le S2
   constraint pb_domainemod check (domainemod in ('Technique','Général','Professionnel'))  
);

CREATE TABLE Competence(
   codeComp VARCHAR(2),
   nomComp VARCHAR(150) not null,
   primary key(codeComp),
   constraint pb_codeComp check (codeComp in ('C1', 'C2', 'C3', 'C4', 'C5', 'C6'))
);

CREATE TABLE Etudiant(
   codeEtu BIGINT,
   nomEtu VARCHAR(150),
   typeBac VARCHAR(50) not null,
   groupe VARCHAR(2) not null,
   primary key(codeEtu, nomEtu),
   --nomEtu en clé primaire également afin de l'avoir dans Evaluer et donc pouvoir importer correctement le csv
   constraint pb_typeBac check (typeBac in ('Techno', 'Général')),
   constraint pb_groupe check (groupe in ('G1', 'G2','G3'))
);


CREATE TABLE UE(
   codeUE VARCHAR(5),
   nomUE VARCHAR(150) not null,
   codeComp VARCHAR(2) not null,
   primary key(codeUE),
   foreign key(codeComp) references Competence(codeComp),
   constraint pb_codeUE check (codeUE in ('UE1.1', 'UE2.1', 'UE1.2', 'UE2.2', 'UE1.3', 'UE2.3', 'UE1.4', 'UE2.4', 'UE1.5', 'UE2.5', 'UE1.6', 'UE2.6'))
   --comme indiqué dans le sujet on se base QUE sur le S1 et le S2
   );

CREATE TABLE Appartenir(
   codemod VARCHAR(5),
   codeUE VARCHAR(5),
   coef INT not null,
   primary key(codemod, codeUE),
   foreign key(codemod) references Modulee(codemod),
   foreign key(codeUE) references UE(codeUE),
   constraint pb_coef check (coef > 0) 
);

CREATE TABLE Evaluer(
   codemod VARCHAR(5),
   note DECIMAL(15,2),
   codeEtu BIGINT,
   nomEtu VARCHAR(150),
   primary key(codemod, codeEtu),
   foreign key(codemod) references Modulee(codemod),
   foreign key(codeEtu, nomEtu) references Etudiant(codeEtu, nomEtu),
   constraint pb_note_intervalle check ((note >= 0) or (note <= 20))
);
