create view MoyenneUE as
	select distinct nometu, codeue, codecomp, round(sum(note*coefressue)/sum(coefressue),2) as moyenneUe
	from etudiant join obtenir using(codeetu)
		join modules using(codemod)
		join mobiliser using(codemod)
		join ue using(codeue)
		join competence using(codecomp)
	group by nometu, codeue, codecomp 
	order by nometu asc;


create view MoyenneComp as
	select nometu,codecomp,round(avg(moyenneUe),2) as moyennecomp,
			rank () over (partition by codecomp order by avg(moyenneUe)desc) as classement,
			count(*) over(partition by codecomp ) as nb_etu,
			case when (round(avg(moyenneUe),2)) >= 10 then 'ACQ'else 'NACQ' end as acquisition
			
	from moyenneue join competence using(codecomp)
	group by nometu,codecomp
	order by nometu,codecomp;

create view bilancompetence as
select nometu, 
max(case when codecomp='C1' then moyennecomp else null end) as c1,
max(case when codecomp='C2' then moyennecomp else null end) as c2,
max(case when codecomp='C3' then moyennecomp else null end) as c3,
max(case when codecomp='C4' then moyennecomp else null end) as c4,
max(case when codecomp='C5' then moyennecomp else null end) as c5,
max(case when codecomp='C6' then moyennecomp else null end) as c6,
least(max(case when codecomp = 'C1' then moyennecomp else null end), 
	max(case when codecomp = 'C2' then moyennecomp else null end), 
	max(case when codecomp = 'C3' then moyennecomp else null end), 
	max(case when codecomp = 'C4' then moyennecomp else null end), 
	max(case when codecomp = 'C5' then moyennecomp else null end), 
	max(case when codecomp = 'C6' then moyennecomp else null end)) as notemin	
from MoyenneComp
group by nometu
order by notemin desc, nometu;
